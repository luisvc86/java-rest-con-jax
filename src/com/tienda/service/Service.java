package com.tienda.service;


import java.util.List;

import javax.ws.rs.core.Response;

import com.tienda.dao.ArticuloDaoImpl;
import com.tienda.pojo.Articulo;
import com.tienda.pojo.Mensaje;



public class Service {

	
	private static ArticuloDaoImpl articulodao;
	static{
		articulodao=new ArticuloDaoImpl();	
	}
	
	
	public Response save(Articulo articulo){
		
		try {
			boolean save = articulodao.save(articulo);
			
			if (save == true) {
				articulo=findByName(articulo.getNombre());
				return Response.status(Response.Status.CREATED).entity(articulo).build();
			} else {
				Mensaje mensaje= new Mensaje();
				mensaje.setError("Bad_Request");
				mensaje.setMessage("No se pudo guardar el usuario");
				return Response.status(Response.Status.BAD_REQUEST).entity(mensaje).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Mensaje mensaje= new Mensaje();
			mensaje.setError("INTERNAL_SERVER_ERROR");
			mensaje.setMessage("Hubo un error interno,por favor vuelva a intentarlo mas tarde");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(mensaje).build();
		}
	}
	
	public Response Update(int id,Articulo articulo) {
		
	   try {
			boolean save = articulodao.update(id, articulo);
			if (save == true) {
				articulo=articulodao.findById(id);
				return Response.status(Response.Status.OK).entity(articulo).build();
			} else {
				Mensaje mensaje= new Mensaje();
				mensaje.setError("Bad_Request");
				mensaje.setMessage("No se pudo actualizar el usuario");
				return Response.status(Response.Status.BAD_REQUEST).entity(mensaje).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Mensaje mensaje= new Mensaje();
			mensaje.setError("INTERNAL_SERVER_ERROR");
			mensaje.setMessage("Hubo un error interno,por favor vuelva a intentarlo mas tarde");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(mensaje).build();
		}
	   
	   
	}

	public Response delete(int idAd) {
		
		try {
			boolean save = articulodao.delete(idAd);
			if (save == true) {
				Mensaje mensaje= new Mensaje();
				mensaje.setError("Ok");
				mensaje.setMessage("El articulo se borro exitosamente");
				return Response.status(Response.Status.OK).entity(mensaje).build();
			} else {
				Mensaje mensaje= new Mensaje();
				mensaje.setError("Bad_Request");
				mensaje.setMessage("No se pudo actualizar el usuario");
				return Response.status(Response.Status.BAD_REQUEST).entity(mensaje).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Mensaje mensaje= new Mensaje();
			mensaje.setError("INTERNAL_SERVER_ERROR");
			mensaje.setMessage("Hubo un error interno,por favor vuelva a intentarlo mas tarde");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(mensaje).build();
		}
	   
	}
	
	public Response findAll() {
		
		try {
			List<Articulo> articulos = articulodao.findAll();
			
			if(articulos!=null && !articulos.isEmpty()){
				return Response.status(Response.Status.OK).entity(articulos).build();
			}else {
				Mensaje mensaje= new Mensaje();
				mensaje.setError("NOT_FOUND");
				mensaje.setMessage("No se pudo encontrar ningun articulo");
				return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Mensaje mensaje= new Mensaje();
			mensaje.setError("INTERNAL_SERVER_ERROR");
			mensaje.setMessage("Hubo un error interno,por favor vuelva a intentarlo mas tarde");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(mensaje).build();
		}
	}


	public Response findById(int id) {
		try {
			Articulo articulo = articulodao.findById(id);
			
			if(articulo!=null && articulo.getIdArticulo()!=0){
				return Response.status(Response.Status.OK).entity(articulo).build();
			}else {
				Mensaje mensaje= new Mensaje();
				mensaje.setError("NOT_FOUND");
				mensaje.setMessage("No se pudo encontrar ningun articulo");
				return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Mensaje mensaje= new Mensaje();
			mensaje.setError("INTERNAL_SERVER_ERROR");
			mensaje.setMessage("Hubo un error interno,por favor vuelva a intentarlo mas tarde");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(mensaje).build();
		}
	}

	public Articulo findByName(String name) {
		return articulodao.findByName(name);
	}

}
