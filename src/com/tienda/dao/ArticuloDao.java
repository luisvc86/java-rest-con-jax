package com.tienda.dao;

import java.util.List;

import com.tienda.pojo.Articulo;

public interface ArticuloDao {
	public boolean save(Articulo articulo);
	public List<Articulo> findAll();
	public Articulo findById(int id);
	public Articulo findByName(String name);
	public boolean update(int id,Articulo articulo);
	public boolean delete(int idAd);

	
}
