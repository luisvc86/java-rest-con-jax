package com.tienda.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tienda.conexion.Conexion;
import com.tienda.pojo.Articulo;

public class ArticuloDaoImpl implements ArticuloDao {


	public ArrayList<Articulo> findAll() {
		ArrayList< Articulo> articulos = new ArrayList<Articulo>();
		Conexion conex = new Conexion();
		     
		  try {
		   PreparedStatement consulta = conex.getConnection().prepareStatement("SELECT * FROM articulo");
		   ResultSet res = consulta.executeQuery();
		   while(res.next()){
			   Articulo articulo= new Articulo();
			   articulo.setIdArticulo(Integer.parseInt(res.getString("idArticulo")));
			   articulo.setNombre(res.getString("nombre"));
			   articulo.setDescripsion(res.getString("descripcion"));
			   articulo.setPrecio(Float.parseFloat(res.getString("precio")));
			   articulo.setModelo(res.getString("modelo"));
			   articulos.add(articulo);
		          }
		          res.close();
		          consulta.close();
		          conex.desconectar();
		    
		  } catch (Exception e) {
		  }
		  return articulos;
		 }
	

	public Articulo findById(int id) {
		Articulo articulo =new Articulo();
		Conexion conex = new Conexion();
		     
		  try {
		   PreparedStatement consulta = conex.getConnection().prepareStatement("SELECT * FROM articulo where idArticulo = ? ");
		   consulta.setInt(1, id);
		   ResultSet res = consulta.executeQuery();
		   if(res.next()){
		   articulo.setIdArticulo(Integer.parseInt(res.getString("idArticulo")));
		   articulo.setNombre(res.getString("nombre"));
		   articulo.setDescripsion(res.getString("descripcion"));
		   articulo.setPrecio(Float.parseFloat(res.getString("precio")));
		   articulo.setModelo(res.getString("modelo"));
		    }
		          res.close();
		          consulta.close();
		          conex.desconectar();
		    
		  } catch (Exception e) {
		  }
		  return articulo;
	}
	
	public boolean save(Articulo articulo) {
		Conexion conex = new Conexion();
		try {
			Statement estatuto = conex.getConnection().createStatement();
			estatuto.executeUpdate("INSERT INTO articulo (descripcion,modelo,nombre,precio) VALUES ('"
					+ articulo.getDescripsion() + "', '" + articulo.getModelo() + "', '" + articulo.getNombre() + "', '"
					+ articulo.getPrecio() + "')");
			estatuto.close();
			conex.desconectar();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}

	}

	public boolean update(int id,Articulo articulo) {
		Conexion conex= new Conexion();
		  try {
			  String query = "update articulo set descripcion = ?,modelo=? where idArticulo = ?";
			PreparedStatement preparedStmt = conex.getConnection().prepareStatement(query);
		      preparedStmt.setString(1,articulo.getDescripsion());
		      preparedStmt.setString(2,articulo.getModelo());
		      preparedStmt.setInt   (3,id);
		      int retorno = preparedStmt.executeUpdate();
		   return true;
		  } catch (SQLException e) {
		   System.out.println(e.getMessage());
		  return false;
		  }
	}

	public boolean delete(int idAd) {
		// TODO Auto-generated method stub
		Conexion conex = new Conexion();
		try {
			Statement estatuto = conex.getConnection().createStatement();
			estatuto.execute("DELETE FROM articulo WHERE idArticulo='"+idAd+"'");
			estatuto.close();
			conex.desconectar();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}
	
	public Articulo findByName(String name) {
		Articulo articulo =new Articulo();
		Conexion conex = new Conexion();
		     
		  try {
		   PreparedStatement consulta = conex.getConnection().prepareStatement("SELECT * FROM articulo where nombre = ? ");
		   consulta.setString(1,name);
		   ResultSet res = consulta.executeQuery();
		   if(res.next()){
		   articulo.setIdArticulo(Integer.parseInt(res.getString("idArticulo")));
		   articulo.setNombre(res.getString("nombre"));
		   articulo.setDescripsion(res.getString("descripcion"));
		   articulo.setPrecio(Float.parseFloat(res.getString("precio")));
		   articulo.setModelo(res.getString("modelo"));
		    }
		          res.close();
		          consulta.close();
		          conex.desconectar();
		    
		  } catch (Exception e) {
		  }
		  return articulo;
	}


}
