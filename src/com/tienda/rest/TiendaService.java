package com.tienda.rest;

import com.tienda.pojo.Articulo;
import com.tienda.service.Service;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.jaxrs.JavaHelp;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/articulo/")
@Api(value = "/articulo", description = "Operaciones con articulos")
@Produces(MediaType.APPLICATION_JSON)
public class TiendaService extends JavaHelp {
	private static Service service;
	static {
		service = new Service();
	}

	@GET
	@Path("/find/all")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation( value = "Devuelve todos los articulos",notes = "Devuelve todos los articulos en el sistema")
	
	public Response findAll() {
		
		return service.findAll();
	}

	@GET
	@Path("/find/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation( value = "Busca un articulo por id",notes = "Devuelve el articulo relativo al id")
	
	public Response findById( 
		//	@ApiParam(value = "ID del articulo a buscar", allowableValues = "range[1," + Integer.MAX_VALUE + "]", required = true)		
			@PathParam("id") int id) {
		
		return service.findById(id);
	}

	@POST
	@Path("/add")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Da de alta un nuevo articulo",notes = "Crea un nuevo articulo ")
	
	public Response addArticulos(
			@ApiParam(value = "Datos del nuevo articulo", required = true)
			Articulo nuevoArticulo) {
		// c�digo
		return service.save(nuevoArticulo);
	}

	@PUT
	@Path("/update/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(
            value = "Actualiza descripci�n y modelo de un articulo ",
            notes = "Actualiza descripci�n y modelo del articulo que  corresponda con el id"
    )
	
	public Response updateArticulos(
			@ApiParam(value = "id del articulo a actualizar", required = true)
			@PathParam("id") int id, 
			@ApiParam(value = "Datos del articulo a actualizar", required = true)
			Articulo articulo) {
		// c�digo
		return service.Update(id, articulo);
	}

	@DELETE
	@Path("/delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(
            value = "Elimina un articulo",
            notes = "Elimina los datos del articulo que  corresponda con el id. El articulo debe existir"
    )
	
	public Response removeArticulos(
			  @ApiParam(value = "ID del articulo a eliminar", allowableValues = "range[1," + Integer.MAX_VALUE + "]", required = true)
			@PathParam("id") int id) {
		return service.delete(id);
	}

}
